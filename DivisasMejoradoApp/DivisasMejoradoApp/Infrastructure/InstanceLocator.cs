﻿using DivisasMejoradoApp.ViewModels;

namespace DivisasMejoradoApp.Infrastructure
{
    public class InstanceLocator
    {
        public HomeViewModel Main { get; set; }

        public InstanceLocator()
        {
            Main = new HomeViewModel();
        }
    }
}
